import React, { useState } from 'react';

export default function CurrencyConverter() {
  const [currencyFrom, setCurrencyFrom] = useState('');
  const [currencyTo, setCurrencyTo] = useState('');
  const [date, setDate] = useState(''); // New state for the date
  const [exchangeRate, setExchangeRate] = useState('Not fetched yet');



  const handleCurrencyFromChange = (event) => {
    setCurrencyFrom(event.target.value.toUpperCase());
  };

  const handleCurrencyToChange = (event) => {
    setCurrencyTo(event.target.value.toUpperCase());
  };

  const handleDateChange = (event) => {
    setDate(event.target.value);
  };

  const fetchExchangeRate = async () => {
    try {
      // Include the date parameter in the API request URL
      const response = await fetch(`http://localhost:8080/financial-basic-data/currency?from=${currencyFrom}&to=${currencyTo}&date=${date}`);
      const data = await response.json();
      console.log(data);
      setExchangeRate(data.exchangeRate);
    } catch (error) {
      console.error('Error fetching exchange rate:', error);
    }
  };

  return (
    <div style={styles.pageContainer}>
      <div style={styles.container}>
        <h1 style={styles.header}>Przelicznik walut</h1>
  
        <div style={styles.inputGroup}>
          <input
            type="text"
            placeholder="Kod waluty (np. EUR)"
            value={currencyFrom}
            onChange={handleCurrencyFromChange}
            style={styles.input}
          />
          <input
            type="text"
            placeholder="Kod waluty (np. PLN)"
            value={currencyTo}
            onChange={handleCurrencyToChange}
            style={styles.input}
          />
          <input
            type="date"
            placeholder="2024-01-25"
            value={date}
            onChange={handleDateChange}
            style={styles.input}
          />
        </div>
  
        <button onClick={fetchExchangeRate} style={styles.button}>
          Pobierz kurs
        </button>
  
        {exchangeRate && (
  <div style={styles.exchangeRateDisplay}>
    Kurs wymiany: {exchangeRate !== 'Not fetched yet' ? exchangeRate : 'Kurs nie został jeszcze pobrany'}
  </div>
)}
      </div>
    </div>
  );
}

// Stylowanie komponentu
const styles = {
  
  pageContainer: {
    display: 'flex',
    alignItems: 'center', // wyśrodkowanie pionowe
    justifyContent: 'center', // wyśrodkowanie poziome
    height: '100vh', // wysokość całego widoku
    backgroundColor: '#f0f0f0', // kolor tła strony
  },
  container: {
    textAlign: 'center',
    marginTop: '50px',
    padding: '20px',
    backgroundColor: '#f2f2f2', // jasnoszary kolor tła
    borderRadius: '15px',
    width: '50%',
    margin: 'auto',
    boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)'
  },
  header: {
    marginBottom: '20px'
  },
  inputGroup: {
    marginBottom: '10px'
  },
  input: {
    marginRight: '10px',
    padding: '10px',
    fontSize: '16px',
    border: '1px solid #ccc',
    borderRadius: '4px'
  },
  button: {
    padding: '10px 20px',
    fontSize: '16px',
    color: 'white',
    backgroundColor: '#007bff',
    border: 'none',
    borderRadius: '4px',
    cursor: 'pointer'
  },
  conversionResult: {
    marginTop: '20px',
    fontSize: '18px',
    color: 'green', // Możesz dostosować styl według własnych preferencji
  },
  exchangeRateDisplay: {
    marginTop: '20px',
    fontSize: '18px',
    color: 'blue', // You can adjust the style as per your preference
    // Add other styling as needed
  }
};